# -*- coding: utf-8 -*-
require 'net/http'
require 'uri'
require 'nokogiri'
require 'kconv'

module Rakuten
  class RequestError < StandardError; end

  class Shop
    VERSION = '0.0.1'

    SERVICE_URL = {
     :xml => "http://api.rakuten.co.jp/rws/3.0/rest?",
     :json => "http://api.rakuten.co.jp/rws/3.0/json?"
    }

    @@options = {
      :version => "2011-12-01",
      :operation => "BooksBookSearch"
    }

      class << self

        def options
          @@options
        end

        def options=(opt)
          if opt[:version]
            @@options[:version] = opt.delete(:version)
          end
          if opt[:operation]
            @@options[:operation] = opt.delete(:operation)
          end
          
          @@options = @@options.merge(opt)

        end

        #default operation is BooksBookSearch
        def search_title(title,opt={})

          if opt[:operation]
            operation = opt.delete(:operation)
            @@options[:operation] = operation
          end

          @@options[:version] = opt[:version] || "2010-09-15"

          opt[:title]=title

          send_request(opt)
        end

        def search(keywd, opt={})
          if opt[:operation]
            operation = opt.delete(:operation)
            @@options[:operation] = operation
          end

          @@options[:version] = opt[:version] || "2010-09-15"

          opt[:keyword] = keywd
          send_request(opt)
        end

        #default operation is BooksBookSearch and this operation need option of operation as BooksBookSearch
        def search_isbn(isbn,opt={})
           if opt[:operation]
             @@options[:operation] = opt.delete(:operation)
           end

           raise Rakuten::RequestError , "You can't [search_isbn] without specifing option of operation as BooksBookSearch."    unless @@options[:operation] == "BooksBookSearch"
           opt[:isbn] = isbn
           send_request(opt)
        end

        def send_request(opts)
         opts = self.options.merge(opts) if self.options

         request_url = prepare_url(opts)
         p "Request URL: #{request_url}"
         res = Net::HTTP.get_response(URI::parse(request_url))
         unless res.kind_of? Net::HTTPSuccess
           raise Rakuten::RequestError, "HTTP Response: #{res.code} #{res.message}"
         end
         

         Response.new(res.body)

       end

        def prepare_url(opts)
          # Wheter I need trancerate character-code to utf
          utf_flag = ["title","author","publisherName","sort"]
          format = opts.delete(:format)
          format = (format.nil?) ? 'xml' : format
          request_url = SERVICE_URL[format.to_sym]
          raise Rakuten::RequestError , "Invalid format '#{format}'" unless request_url

          developerId = opts.delete(:developerId)
          raise Rakuten::RequestError , "You can't request without specifing [developerId]" unless developerId

          url_body = ''
            
          opts.each do |e|
            p "Adding #{e[0]}=#{e[1]}"
            next unless e[1]
            value = e[1]
            category = e[0]
             if utf_flag.include? category
               category = Kconv.toutf8(category)
             end
            url_body << "&"
            url_body <<  "#{category}=#{value}"
          end

          return "#{request_url}developerId=#{developerId}#{url_body}" 
          
        end

      end

    class Response
      def initialize(xml)
        @doc = Nokogiri::XML(xml, nil, 'UTF-8')
      end

      def items
       @items ||= (@doc/"Item").collect do |item|
                      Element.new(item)
                  end
      end

    end

    class Element
      class << self
        def get(element,path=".")
          return unless element
          result = element.xpath(path).inner_html rescue nil
        end
      end

      def initialize(element)
        @element = element
      end

      def get(path=".")
        Element.get(@element,path)
      end

    end # class Element

  end # class Shop
end # module Rakuten

