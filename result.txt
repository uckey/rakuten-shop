実行処理

Rakuten::Shop.options={
  :developerId => "Your developerId",
  :affiliateId => "Your affiliateId" 
}

res = Rakuten::Shop.search_title "ruby",:sort => "-releaseDate" 

items = res.items
item = items.first
p item.get("title")
p item.get("titleKana")
p item.get "seriesName"
p item.get "seriesNameKana"
p item.get "author"
p item.get "authorKana"
p item.get "publisherName"
p item.get "size"
p item.get "isbn"
p item.get "itemCaption"
p item.get "salesDate"
p item.get "itemPrice"
p item.get "affiliateUrl"


ーーーーーーーーーーーーーーーーーーーーーーーー


"Rubyについて検索"
"Adding version=2011-12-01"
"Adding operation=BooksBookSearch"
"Adding title=ruby"
"Request URL: http://api.rakuten.co.jp/rws/3.0/rest?developerId=<Your-developerId>&version=2011-12-01&operation=BooksBookSearch&affiliateId=<Your-affiliateId>&title=ruby"

p item.get("title")
"Ruby　on　Rails　3ポケットリファレンス"

p item.get("titleKana")
"ルビー オン レイルズ サン ポケット リファレンス"

p item.get "seriesName"
"Ｐｏｃｋｅｔ　ｒｅｆｅｒｅｎｃｅ"

p item.get "seriesNameKana"
"ポケット リファレンス"

p item.get "author"
"山田祥寛"

p item.get "authorKana"
"ヤマダ,ヨシヒロ"

p item.get "publisherName"
"技術評論社"

p item.get "size"
"単行本"

p item.get "isbn"
"9784774149806"


p item.get "itemCaption"
"１３年のロングセラーシリーズ・ポケットリファレンスにＷｅｂアプリケーションフレームワークの新定番であるＲａｉｌｓ３が新登場。「逆引き形式で目的からすぐ探せる」「サンプルコードを見ながら具体的な実装のイメージがつかめる」という特徴で、困ったときにすぐに役立ちます。Ｒａｉｌｓ３．１以降に対応し、標準ライブラリとなったｊＱｕｅｒｙやＣｏｆｆｅｅＳｃｒｉｐｔの話題もフォロー。定番解説書である『Ｒｕｂｙ　ｏｎ　Ｒａｉｌｓ３アプリケーションプログラミング』の著者・山田祥寛氏による、現場で役立つ信頼の１冊。"


p item.get "salesDate"
"2012年03月"

p item.get "itemPrice"
"2919"

p item.get "affiliateUrl"
"http://hb.afl.rakuten.co.jp/hgc/0fa42749.4f357875.0fa4274a.d985561e/?pc=http%3A%2F%2Fbooks.rakuten.co.jp%2Frb%2F11485720%2F&amp;m=http%3A%2F%2Fm.rakuten.co.jp%2Frms%2Fmsv%2FItem%3Fn%3D11485720%26surl%3Dbook"
